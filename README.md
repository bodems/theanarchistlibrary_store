# Calibre store plugin for The Anarchist Library
This is a working Calibre store plugin for The Anarchist Library. The original plugin, which you can find here, https://bookshelf.theanarchistlibrary.org/library/calibre-store-plugin was broken and the repository was unavailable, so I fixed it.

This plugin is for the English Anarchist Library, but it should also work with other Anarchist Libraries (or other Musewiki instances), if you adjust the URLs. If you want to use it, just download this repo as .zip and install it as explained at https://bookshelf.theanarchistlibrary.org/library/calibre-store-plugin
