__license__ = 'GPL 3'
__copyright__ = '2012, Ruben Pollan <meskio@sindominio.net>; 2020, bodems <codeberg@bodems.net>'
__docformat__ = 'restructuredtext en'

import urllib2
from contextlib import closing

import json

from PyQt5.Qt import QUrl

from calibre import browser
from calibre.gui2 import open_url
from calibre.gui2.store import StorePlugin
from calibre.gui2.store.basic_config import BasicStoreConfig
from calibre.gui2.store.search_result import SearchResult
from calibre.gui2.store.web_store_dialog import WebStoreDialog

class TheAnarchistLibraryStore(BasicStoreConfig, StorePlugin):

    def open(self, parent=None, detail_item=None, external=False):
        url = 'https://theanarchistlibrary.org/'

        if external or self.config.get('open_external', False):
            open_url(QUrl(url_slash_cleaner(detail_item if detail_item else url)))
        else:
            d = WebStoreDialog(self.gui, url, parent, detail_item)
            d.setWindowTitle(self.name)
            d.set_tags(self.config.get('tags', ''))
            d.exec_()

    def search(self, query, max_results=10, timeout=60):
        url = 'https://theanarchistlibrary.org/search?fmt=json&query=' + urllib2.quote(query)

        br = browser()

        counter = max_results
        with closing(br.open(url, timeout=timeout)) as f:
            doc = json.load(f)
            for data in doc:
                s = SearchResult()
                s.title = data['title'].strip()
                s.author = data['author'].strip()
                s.price = '$0.00'
                s.detail_item = data['url'].strip()
                s.drm = SearchResult.DRM_UNLOCKED
                s.downloads['EPUB'] = data['url'].strip() + '.epub'
                s.downloads['PDF'] = data['url'].strip() + '.pdf'
                s.downloads['A4.PDF'] = data['url'].strip() + '.a4.pdf'
                s.downloads['LT.PDF'] = data['url'].strip() + '.lt.pdf'
                s.downloads['TXT'] = data['url'].strip() + '.txt'
                s.downloads['TEX'] = data['url'].strip() + '.tex'
                s.downloads['MUSE'] = data['url'].strip() + '.muse'
                s.formats = 'EPUB, PDF, A4.PDF, LT.PDF, TXT, TEX, MUSE'

                yield s
